# Классы Board, BoardTile, BoardTileModifier

Расположены в `Source/Board`
## `Board` Класс

Основной класс, который хранится на сервере и который имеет внутри себя остальные. Задает, собственно доску лабиринта.

Доска лабиринта хранится в виде одномерного массива, эмулирующего двумерный. Если в функции обращение по X, Y, то 0, 0 можно считать верхний левый угол, то есть третья четверть координат.

Является GameStateComponent для правильной репликации, однако написан как чистый C++ класс
### Синтакс

```
UCLASS()
class CANDELA_PROJECT_API UBoard : public UGameStateComponent
```

### Поля и методы
#### Статические поля
|Имя|Описание|
|----|------------|
|`int32 Size`|Размер одного измерения поля, значение 10|
|`int32 NumberOfTargets` | Количество тайлов типа ETileType::Target

#### Приватные поля
| Имя                            | Описание                                                              | Replicated? |
|--------------------------------|-----------------------------------------------------------------|---|
| `TArray<UBoardTile*> Tiles`     | Одномерный массив Тайлов, иммитирующий двумерный по принципу i*size+j |Да|
| `TArray<int32> TargetIndexes` | Одномерный массив индексов тайлов типа ETileType::Target              |Да|
| `int32 SpawnIndex`            | Индекс Спавна в Массиве, по умолчанию 0                               |Да|
| `TArray<EDoorPosition> VerticalDoors` | Массив вертикальных проходов, хранит их положение |Да|
| `TArray<EDoorPosition> HorizontalDoors` | Массив горизонтальных проходов, хранит их положение |Да|

#### Конструкторы

|Имя|Аргументы |Описание|
|----------|---------|-----------------|
|`explicit UBoard(const FObjectInitializer& ObjectInitializer);`|-|Создает объекты клеток с типом `ETileType::Empty`, аргумент это внутренняя вешь Анрила, которая передается автоматчиески|

#### Публичные Методы

| Name                                                    | Description                                                   |
|---------------------------------------------------------|---------------------------------------------------------------|
|[`UBoard::GetTilesArray`](#gettilesarray) | Возвращает константную ссылку на массив тайлов |
|[`UBoard::GetTile`](#gettile) | Возвращает тайл по индексу |
|[`UBoard::GetCoords`](#getcoords) | Возвращает пару координат по индексу |
|[`UBoard::GetTileModifiers`](#gettilemodifers) | Возвращат модификатор тайла по координатам |
| [`UBoard::GetTileIndex`](#gettileindex)                 | Получает индекс в двумерном массиве по абсолютным координатам |
| [`UBoard::GetTileIndexRelativeIndex`](#gettileindexrelativeindex) | Получает индекс относительно спавна по индексу                           |
| [`UBoard::GetTileIndexRelativeCoords`](#gettileindexrelativecoords) | Получает индекс относительно спавна по координатам                           |
| [`UBoard::GetHorizontalDoorsIndex`](#gethorizontaldoorsindex)| Возращает индекс в массиве горизонтальных дверей |
| [`UBoard::GetTopDoor`](#gettopdoor)| Возращает статус верхнего прохода по координатам |
| [`UBoard::GetDownDoor`](#getdowndoor)| Возращает статус нижнего прохода по координатам |
| [`UBoard::GetRightDoor`](#getrightdoor)| Возращает статус правого прохода по координатам |
| [`UBoard::GetLeftDoor`](#getleftdoor)| Возращает статус левого прохода по координатам |
| [`UBoard::GetVerticalDoors`](#getverticaldoors)| Возращает константую ссылку на массив вертикальных дверей |
| [`UBoard::GetHorizontalDoors`](#gethorizontaldoors)| Возращает константную ссылку на массив горизонтальных дверей |
| [`UBoard::GetSize`](#getsize)| Возращает размер |
| [`UBoard::GetNumberOfTargets`](#getnumberoftargets)| Возращает количество таргетов |
| [`UBoard::LogEntireMap`](#logentiremap)| Вывод всей карты в UE_LOG(), Дебажная функция |
| [`UBoard::SetTileModifiers`](#settilemodifiers)| Поставить клетке модификаотры |
| [`UBoard::Reset`](#reset)                               | Ставит все клетки на `ETileType::Empty`                       |
| [`UBoard::InitBoard`](#initboard)                       | Ресетает доску и ставит спавн в центр, а также делает первое расширение                         |
| [`UBoard::InitTarget`](#inittarget)                     | Создаёт number_of_targets полей типа ETileType::Target        |
| [`UBoard::BuildPathToTarget`](#buildpathtotarget)       | Строит диагональный путь из спавна в заданную точку           |
| [`UBoard::Expansion`](#expansion)                       | Пытаеться расширить лабиринт в случайном напревлении          |

### Наследование
`UGameStateComponent`

### Требования

**Header:** `Board.h`

---

### <a name="gettilesarray"></a> `UBoard::GetTilesArray`

Используйте эту функцию чтобы получить весь массив тайлов по ссылке.

```
UFUNCTION()
const TArray<UBoardTile*>& GetTilesArray() const;
```

#### Возвращаемое значение
Ссылка на массив доски

---
### <a name="gettile"></a> `UBoard::GetTile`

Используйте эту функцию чтобы получить одну клетку в массиве по индексу

```
UFUNCTION()
const UBoardTile* GetTile(const int32 Index) const;
```

#### Параметры

*`Index`*   
индекс

#### Вовзращаемое значение
Константая ссылка на клетку по индексу

---
### <a name="gettileindex"></a> `UBoard::GetTileIndex`

Используйте эту функцию чтобы получить индекс в массиве тайлов по координатам.

```
UFUNCTION()
static int32 GetTileIndex(const int32 Row, const int32 Column);
```

#### Параметры

*`Row`*   
Ряд

*`Column`*  
Столбец
#### Возвращаемое значение
int32 индекса клетки




---
### <a name="getcoords"></a> `UBoard::GetCoords`

Используйте эту функцию чтобы получить координаты в массиве тайлов по индексу.

```
static Chaos::Pair<int32, int32> GetCoords(const int32 Index);
```

#### Параметры

*`Index`*   
Ряд

#### Возвращаемое значение
Пара X и Y





---
### <a name="gettilemodifiers"></a> `UBoard::GetTileModifiers`

Используйте эту функцию чтобы получить модификаторы клетки по координатам

```
UFUNCTION()
TArray<UBoardTileModifier*> GetTileModifiers(const int32 X, const int32 Y) const;
```

#### Возвращаемое значение
Массив модификаторов








---
### <a name="gethorizontaldoorsindex"></a> `UBoard::GetHorizontalDoorsIndex`

Используйте эту функцию чтобы по индексу клетки получить индекс двери в массиве горизонтальных дверей.

Получается дверь ближе к 0.

Для вертикальных используйте `GetTileIndex()`

```
UFUNCTION()
static int32 GetHorizontalDoorsIndex(const int32 X, const int32 Y);
```








---
### <a name="gettopdoor"></a> `UBoard::GetTopDoor`

Получает тип верхней двери.

```
UFUNCTION()
EDoorPosition GetTopDoor(const int32 X, const int32 Y) const;
```

#### Возвращаемое значение
Enum EDoorPosition


---
### <a name="getdowndoor"></a> `UBoard::GetDownDoor`

Получает тип нижней двери.

```
UFUNCTION()
EDoorPosition GetDownDoor(const int32 X, const int32 Y) const;
```

#### Возвращаемое значение
Enum EDoorPosition




---
### <a name="getrightdoor"></a> `UBoard::GetRightDoor`

Получает тип правой двери.

```
UFUNCTION()
EDoorPosition GetRightDoor(const int32 X, const int32 Y) const;
```

#### Возвращаемое значение
Enum EDoorPosition




---
### <a name="getleftdoor"></a> `UBoard::GetLeftDoor`

Получает тип левой двери.

```
UFUNCTION()
EDoorPosition GetLeftDoor(const int32 X, const int32 Y) const;
```

#### Возвращаемое значение
Enum EDoorPosition









---
### <a name="getverticaldoors"></a> `UBoard::GetVerticalDoors`

Получить константную ссылку на массив вертикальных дверей. В основном нужно для тестов.

```
const TArray<EDoorPosition>& GetVerticalDoors();
```

---
### <a name="gethorizontaldoors"></a> `UBoard::GetHorizontalDoors`

Получить константную ссылку на массив горизонтальных дверей. В основном нужно для тестов.

```
	const TArray<EDoorPosition>& GetHorizontalDoors();

```







---
### <a name="getsize"></a> `UBoard::GetSize`

Получить размер

```
UFUNCTION()
static int32 GetSize();
```








---
### <a name="getnumberoftargets"></a> `UBoard::GetNumberOfTargets`

Получить число целей для прохождения

```
UFUNCTION()
static int32 GetNumberOfTargets();
```






---
### <a name="logentiremap"></a> `UBoard::LogEntireMap`

Выводит всю карту

```
UFUNCTION()
void LogEntireMap() const;
```





---
### <a name="settilemodifiers"></a> `UBoard::SetTileModifiers`

Поставить модификаторы тайлу

```
UFUNCTION()
void SetTileModifiers(const int32 X, const int32 Y, TArray<UBoardTileModifier*> Modifiers);
```





---

### <a name="gettileindexrelativeindex"></a> `UBoard::GetTileIndexRelativeIndex`

Используйте эту функцию чтобы получить индекс в массиве тайлов по координатам относительно индекса

```
UFUNCTION()
static int32 GetTileIndexRelativeIndex(const int32 Index, const int32 OffsetX, const int32 OffsetY);
```

#### Параметры
*`Index`*
Индекс

*`OffsetX`*  
Отклоенение по оси X(двигаемся по одной строке)

*`OffsetY`*  
Отклоенение по оси Y
#### Возвращаемое значение
int32 индекса клетки


---
### <a name="gettileindexrelativecoords"></a> `UBoard::GetTileIndexRelativeCoords`

Используйте эту функцию чтобы получить индекс в массиве тайлов по координатам относительно координат

```
UFUNCTION()
static int32 GetTileIndexRelativeCoords(const int32 X, const int32 Y, const int32 OffsetX, const int32 OffsetY);
```

#### Параметры

*`X`*  
Начальная коордианта X

*`Y`*  
Начальная координата Y

*`OffsetX`*  
Отклоенение по оси X(двигаемся по одной строке)

*`OffsetY`*  
Отклоенение по оси Y
#### Возвращаемое значение
int32 индекса клетки

---
### <a name="reset"></a> `UBoard::Reset`

Используйте эту функцию чтобы очистить доску (важно: клетка спавна тоже пропадает)

```
UFUNCTION()
void Reset();
```

---

### <a name="initboard"></a> `UBoard::InitBoard`

Используйте эту функцию чтобы очистить доску и поставить спавн в центр, а также в первый раз ее расширить.

```
UFUNCTION()
void InitBoard();
```

---
### <a name="inittarget"></a> `UBoard::InitTarget`

Используйте эту функцию чтобы создать поля типа ETileType:Target.

```
UFUNCTION()
void InitTarget();
```
---
### <a name="expansion"></a> `UBoard::Expansion`

Пытаеться расширить лабиринт из заданной клетки

```
UFUNCTION()
bool Expansion(const int32 TargetIndex);
```
#### Параметры

*`TargetIndex`*   
Поле из которого функция пытаеться расширить лабиринт

#### Возвращаемое значение
bool удалось ли расширить лабиринт

---
### <a name="buildpathtotarget"></a> `UBoard::BuildPathToTarget`

Строит диагональный путь из заданной клетки в спавн

```
UFUNCTION()
void BuildPathToTarget(const int32 TargetIndex) const;
```
#### Параметры

*`TargetIndex`*  
Клетка в которую строим путь

---

## `BoardTile` Класс
Хранит информацию о одной клетке, используется только в классе `Board`
### Синтаксис

```
UCLASS()
class CANDELA_PROJECT_API UBoardTile : public UGameStateComponent
```

### Поля и методы
#### Приватные поля
|Имя|Описание|
|----|-----------------|
|`TEnumAsByte<ETileType> Type`|Тип клетки|
|`TArray<UBoardTileModifier*> Modifiers`|Модификаторы клетки|

#### Конструкторы

|Имя|Аргументы |Описание|
|----------|---------|-----------------|
|`UBoardTile::UBoardTile`| const FObjectInitializer& ObjectInitializer|Создает клетку без модификаторов с типом `ETileType::Empty`|

#### Публичные Методы

|Name|Description|
|----------|-----------------|
|[`UBoard::SetType`](#settype)|Устанавливает тип на желаемый|
|[`UBoard::GetType`](#gettype)|Получает тип клетки|

### Наследование
`UGameStateComponent`

### Требования

**Header:** `BoardTile.h`

---
### <a name="settype"></a> `UBoard::SetType`

Используйте эту функцию чтобы поставить тип клетки

```
UFUNCTION()
void SetType(const ETileType NewType)
```

#### Параметры

*`NewType`*   
Новый тип

---
### <a name="gettype"></a> `UBoard::GetType`

Используйте эту функцию чтобы поставить тип клетки

```
UFUNCTION()
ETileType GetType() const;
```

#### Возвращаемое значение
`ETileType` типа клетки



## `BoardTileModifier` Класс

На данный момент пустой

## `ETileType` Enum

|Элемент|Значение|
|----|----------|
|Empty|Нет комнаты|
|Basic|Основная комната|
|Spawn|Спавн игроков|
|Exit|Выход|
|Target|Комнаты-цели, которые нужно посетить, чтобы открылся выход|