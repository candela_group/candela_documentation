# Pawn CameraPawn

Расположен в  `Source/TopDownCamera`
Класс выполняющий роль DefaultPawnClass в GameMod, отвечающий за Top-Down вид камеры во время игры за столом
### Синтакс

```
UCLASS()
class CANDELA_PROJECT_API ACameraPawn : public APawn
```
---
### Поля
|Имя| Описание                                                                                                         |
|-|------------------------------------------------------------------------------------------------------------------|
|`USceneComponent* SceneComponent`| Компонент Сцены(?)                                                                                               |
|`USpringArmComponent* SpringArmComponent`| USpringArmComponent                                                                                              |
|`UCameraComponent* CameraComponent`| Компонент Камеры                                                                                                 |
|`FVector TargetLocation`| FVector отвечающий за перемещение камеры в пространстве                                                          |
|`float TargetZoom`| Отвечает за приближение камеры к игроку                                                                          |
|	`FRotator TargetRotation`| Так как поворот камеры не реализован(а зачем?), то необходим, чтобы инициализировать изначальное положение камеры |
|`float MoveSpeed`| Скорость движения камеры                                                                                         |
|`float MinZoom`| Минимальное расстояние приближения камеры                                                                        |
|`float MaxZoom`| Максимальное расстояние приближения камеры                                                                       |
|`float ZoomSpeed`| Скорость приближения камеры                                                                                      |
---
### Методы
| Имя                                                       | Описание                                           |
|-----------------------------------------------------------|----------------------------------------------------|
| [`void CameraBound`](#camerabound)                        | метод отвечающий за движение камеры вслед за Actor |
| [`void Forward`](#forward)                                | Метод выполняемый при изменении оси "Forward"      |
| [`void Zoom`](#zoom)                                      | Метод выполняемый при изменении оси "Right"        |
| [`void Right`](#right)                                    | Метод выполняемый при изменении оси "Zoom"         |
---
### Наследование
`APawn`

---
### Требования

**Header:** `CameraPawn.h`

---

### <a name="camerabound"></a> `ACameraPawn::CameraBound`

Используйте эту функцию изменить положение камеры.

```
UFUNCTION()
void CameraBound();
```
---

### <a name="forward"></a> `ACameraPawn::Forward`

Используйте, чтобы менять Движение по оси Forward

```
UFUNCTION()
void Forward(float AxisValue);
```

#### Параметры

*`AxisValue`*
Изменение по оси

---

### <a name="zoom"></a> `ACameraPawn::Zoom`

Используйте, чтобы менять Движение по оси Zoom.

```
UFUNCTION()
void Zoom(float AxisValue);
```

#### Параметры

*`AxisValue`*
Изменение по оси


---

### <a name="right"></a> `ACameraPawn::Right`

Используйте, чтобы менять Движение по оси Right.

```
UFUNCTION()
void Right(float AxisValue);
```

#### Параметры

*`AxisValue`*
Изменение по оси


---