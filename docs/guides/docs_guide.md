# Как писать документацию?
Документация собрана на [MkDocs](https://www.mkdocs.org/), написана на [Markdown](https://ru.wikipedia.org/wiki/Markdown), а стоит на [GitLab](https://gitlab.com).
## Установка MkDocs
Нужен pip, с его помощью устанавливаем MkDocs:
```
pip install mkdocs
```

Если после этого не работает, то нужно добавить расширения pip в path.
## Получаем файлы
Если не склонен, клонируем репозиторий:
```
git clone https://gitlab.com/candela_group/candela_documentation
```
Переходим в него:
```
cd Candela_documentation
```
## Запуск локально
MkDocs позволяет локально запустить сервер и real-time видеть изменения в файле. Удостовертесь, что вы уже в папке и запустите сервер командой:
```
mkdocs serve
```
Вам покажет ip-шник, на котором будет наша вики.
## Изменение и добавление файлов
В папке `docs/` лежат все наши файлы по категориям, можете переходить и менять.

Если хочется добавить файл, то добавляем md в соответствующую категорию в папке `docs/` и меняем `mkdocs.yml`, нужно добавить в навигацию наш файл.

```
nav:
  - Home: 'index.md'
  - 'User Guide':
    - 'Writing your docs': 'writing-your-docs.md'
    - 'Styling your docs': 'styling-your-docs.md'
  - About:
    - 'License': 'license.md'
    - 'Release Notes': 'release-notes.md'
```

На первом слое - категории, на втором - документы. Можно добавлять вне категорий, например, если мы хотим добавить категорию `documentation` и документ `tile_board.md`, то nav будет выглядеть

```
nav:
  - Home: 'index.md'
  - 'User Guide':
    - 'Writing your docs': 'writing-your-docs.md'
    - 'Styling your docs': 'styling-your-docs.md'
  - About:
    - 'License': 'license.md'
    - 'Release Notes': 'release-notes.md'
  - Documentation:
    - 'Tile Board Class': 'tile_board.md'
```
## Как залить
Если проверили на локальном сервере `mkdocs serve`, то делаем `git commit`, `git push`;
## Доп. ссылки
* [Документация MkDocs](https://www.mkdocs.org/user-guide/)