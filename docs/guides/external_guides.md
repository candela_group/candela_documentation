#Основные ссылки будет лежать здесь, заглядывайте чаще
## Coding Standards

[Unreal Engine code-style guidelines](https://docs.unrealengine.com/5.3/en-US/epic-cplusplus-coding-standard-for-unreal-engine/)

[Naming Conventions in Unreal Engine](https://docs.unrealengine.com/5.3/en-US/recommended-asset-naming-conventions-in-unreal-engine-projects/)
