# Как писать тесты?
В анриле есть несколько видов тестов, для юнит-тестирования мы используем `Automation tests` 

## Как создать тестирующий файл?
Нужно создать пустой класс, удалить его полностью, сам класс нам не нужен, мы сгенерируем новый. Оставьте `.h` файл, для сложных тестов там полезно создавать вспомогательные функции и классы.

Добавляем `#include "Misc/AutomationTest.h"` в начало файла, после этого инклюдим все тестируемые/требуемые классы.

После всех индклюдов остальной код пишется обернутый в:
```c++
#if WITH_AUTOMATION_TESTS
    ...
#endif // WITH_AUTOMATION_TESTS
```
## Как создавать тесты?
В нашем Ифе чтобы создать один Тест пишем
```c++
IMPLEMENT_SIMPLE_AUTOMATION_TEST(U<NAME>Test, "<OUR>.<PATH>.<TOTEST>", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)
```
Что означают разные флаги - можно понять перейдя к их исходному коду, если просто - они определяют к какому типу нагрузки тест относится и сколько ресурсов требует, флаги обязательны.

Теперь у нас создался класс `U<NAME>Test`, сам тест пишется функцией
```c++
bool UBoardTest::RunTest(const FString& Parameters)
{
    // ...
    return true;
}
```
Если функция возращает true - тест удался, иначе false. Есть разные макросы, которые сами делают проверки, выводят сообщения и выбрасывают функции, о их использовании в стиле.

Если что-то непонятно можно посмотреть пример тестов в `Source/Private/Board/BoardTests.cpp`.
## Как запускать тесты?
Справа в `Rider` - Unit Tests, выбираете тесты проекта, запускаете (их можно дебажить ещё, очень удобная вещь).
## Как иметь доступ к приватным полям класса?
Пусть у нас есть `Class.h` и `ClassTests.cpp`. Наверху `Class.h` добавляем:
```c++
#ifndef CLASS_MAIN_TEST_FRIEND
#define CLASS_MAIN_TEST_FRIEND
#endif
```
(вместо CLASS ваше имя класса)

В сам класс добавялем:
```c++
UCLASS()
class UClass : public UObject
{
	GENERATED_BODY()
	CLASS_MAIN_TEST_FRIEND
    ...
}
```
**Перед** инклюдом `Class.h` в `ClassTests.cpp` добавляем:
```c++
#ifdef CLASS_MAIN_TEST_FRIEND
#undef CLASS_MAIN_TEST_FRIEND
#endif
#define CLASS_MAIN_TEST_FRIEND friend class UClassTest;
```

Теперь наш класс, если был заинклюжен с теста, становится его другом и тест может видеть его поля
## Стиль наших тестов
Файлы называем `<NAME>Tests.cpp`, где `<NAME>` - имя тестируемого класса.

Создаем по тесту на фичу или группу фич, в среднем в одном тесте должно быть не больше 5-7 проверок разного типа.

Используем макросы `Test...` например `TestTrue`, они сильно облегчают проверки, при чем для каждого подтеста пишем его номер в квадратных скобочках и комментарием описываем что за тест:
```c++
// 3. Reset test
Board->Tiles[5]->Type == ETileType::Exit;
Board->Reset();
TestTrue("[3] Some tile reset test", Board->Tiles[5]->Type == ETileType::Empty);
TestTrue("[3] Resets Spawn Index", Board->SpawnIndex == 0);
```