#Здесь будут лицензии на сторонние ассеты
## Sketchfab
- [Halloween Creature](https://sketchfab.com/3d-models/halloween-creature-75586b62dbde450ea459d514750f8a16) - [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/)
- [Dice](https://sketchfab.com/3d-models/dice-c961fb8672df49d5a7374a4320c31200) - [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/)
- [Old Map](https://sketchfab.com/3d-models/old-map-3d-model-c8f056e093f74be99b2e83034099c40a) - [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/)

## Assets from Artstation
- [15 Free Alphas + UE4 Decals Pack](https://www.artstation.com/marketplace/p/ky9R/15-free-alphas-ue4-decals-pack) Extended Commercial License
## FreePik licences
- [Tarot cards](https://www.freepik.com/free-vector/hand-drawn-tarot-cards-illustration_42114115.htm#page=2&query=vintage%20playing%20cards&position=44&from_view=keyword&track=ais_user&uuid=b7d6b38c-3315-460b-ad6e-6c81e600e1c5) - Freepik free license
## Unreal Engine Assets
- [Bridge by Quixel](https://quixel.com/) - Unreal Engine Content License
## Itch.Io Assets
- [Sounds](https://darkworldaudio.itch.io/sound-effects-survival-i) - Custom Open License